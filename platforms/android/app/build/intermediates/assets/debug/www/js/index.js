/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

//var day_name=new Array(7);
//
//day_name[0]='Sunday'
//day_name[1]=' Monday'
//day_name[2]='Tuesday'
//day_name[3]='Wednesday'
//day_name[4]='Thursday'
//day_name[5]='Friday'
//day_name[6]='Saturday'
//
//function display_c(){
//var refresh=1000; // Refresh rate in milli seconds
//mytime=setTimeout('display_ct()',refresh)
//}
//
//function display_ct() {
//var strcount
//var x = new Date();
//document.getElementById('ct').innerHTML = x;
//var x1=x.toUTCString();// changing the display to UTC string
//var gmt=new Date();
//var offSet=x.getTimezoneOffset(); 
//gmt.setMinutes(x.getMinutes()+offSet);
//document.getElementById('ct1').innerHTML = gmt.getHours() + ":" +gmt.getMinutes() + ":" + gmt.getSeconds();
//
////////////////////////////
//function city_zone(offSet){
//var t1=new Date();
//var offSet1=t1.getTimezoneOffset(); 
//t1.setMinutes(t1.getMinutes()+offSet1); // arrive at GMT tme
//t1.setMinutes(t1.getMinutes()+offSet);   // add the city time offset 
//var str=t1.getMonth() + 1;  
//return t1.getHours() + ":" +t1.getMinutes() + ":" + t1.getSeconds() + " <i>Day:</i> "  + day_name[t1.getDay()] + ", <i>Date :</i> " + t1.getDate() + " <i>Month :</i> " +  str;
//}
////////////////////
//
//// Go for different City time ///
//
//document.getElementById('ct2').innerHTML = city_zone(-420); // Arizona 
////document.getElementById('ct3').innerHTML = city_zone(-480); // California
////document.getElementById('ct4').innerHTML = city_zone(600); // New South Wales
//
//
//tt=display_c();
//}

//function AutoRefresh( t ) {
//               setTimeout("location.reload(true);", t);
//            }

function updateClock() {
    var now = new Date(), // current date
        months = ['January', 'February', '...']; // you get the idea
        time = now.getHours() + ':' + now.getMinutes()+ ':' + now.getSeconds // again, you get the idea
//    console.log(now);

        // a cleaner way than string concatenation
        date = [now.getDate(), 
                months[now.getMonth()],
                now.getFullYear()].join(' ');

    // set the content of the element with the ID time to the formatted string
    document.getElementById('time').innerHTML = now; //[date, time].join('   ');

    // call this function again in 1000ms
    setTimeout(updateClock, 1000);
}
updateClock(); // initial call

//var today = new Date();
//var refresh=1000; // Refresh rate in milli seconds
//mytime=setTimeout(new Date(),refresh);

//
//document.getElementById('dtText').innerHTML="Today's Date is :"+today;

var app = {
    // Application Constructor
    initialize: function() {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
};

app.initialize();